﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeatJudger : MonoBehaviour
{
    public float FractionOfBeatForGood;
    public float FractionOfBeatForGreat;

    public enum Judgement
    {
        Great,
        Good,
        Bad
    }

    // Start is called before the first frame update
    void Start()
    {
        //Select Difficulty, easy 90bmp, normal 120bmp, hard 145bpm
        //easy = beat every .75 seconds, normal = .5 seconds

        //Base Score is used to calculate how close the player is to the beat by converting it to a better range.
        // great score is 0.325 - 0.425
        //good score is 0.225 - 0.525
        // bad is 0 - 0.750
    }
    // Update is called once per frame
    void Update()
    {
        bool PlayerClicked = Input.GetMouseButtonDown(0);
        if (PlayerClicked)
        {
            Judgement judgement = GetJudgement();
            float scoreFromClick = 200;
            switch (judgement)
            {
                case Judgement.Great:
                    scoreFromClick = scoreFromClick * 5;
                    Debug.Log("Great!");
                    break;
                case Judgement.Good:
                    Debug.Log("Good!");
                    break;
                case Judgement.Bad:
                    scoreFromClick = 0;
                    Debug.Log("Bad!");
                    break;
            }
            
            Scoring.AddScore((int) scoreFromClick);
        }
    }

    public Judgement GetJudgement()
    {
        if (Beat.Instance.TimeToNearestBeat < Beat.Instance.SecondsPerBeat * FractionOfBeatForGreat)
        {
            return Judgement.Great;
        }
        else if (Beat.Instance.TimeToNearestBeat < Beat.Instance.SecondsPerBeat * FractionOfBeatForGood)
        {
            return Judgement.Good;
        }
        else
        {
            return Judgement.Bad;
        }
    }
}
