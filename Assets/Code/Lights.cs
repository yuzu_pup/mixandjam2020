﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Lights : MonoBehaviour
{
    public float WobbleAngle = 5f;
    public static Lights Instance;

    public Color BlueColor;
    public Color OrangeColor;
    public Color PinkColor;
    public Color PurpleColor;

    public float StartingAlpha;
    public float EndingAlpha;
    public float Alpha;

    private Color ChosenColor;

    public Image[] LightsArr;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        foreach (Image i in LightsArr)
        {
            i.transform.DORotate(new Vector3(0, 0, WobbleAngle), Beat.Instance.SecondsPerBeat * 2)
                            .From(new Vector3(0, 0, -WobbleAngle))
                            .SetEase(Ease.InOutQuad)
                            .SetLoops(-1, LoopType.Yoyo)
                            .Play();
        }

        DOTween.To
        (
            () =>
            {
                return Alpha;
            },
                (t) =>
            {
                Alpha = t;
            }, 
            EndingAlpha, 
            Beat.Instance.SecondsPerBeat
        )
            .From(StartingAlpha)
            .SetLoops(-1)
            .SetEase(Ease.OutCubic);
    }

    private void Update()
    {
        foreach (Image i in LightsArr)
        {
            i.color = new Color(ChosenColor.r, ChosenColor.g, ChosenColor.b, Alpha);
        }
    }

    public static void SetColors(BallColor c)
    {
        Instance.SetColorsIntl(c);
    } 

    public void SetColorsIntl(BallColor color)
    {
        switch(color)
        {
            default:
                ChosenColor = Color.white;
                break;
            case BallColor.BLUE:
                ChosenColor = BlueColor;
                break;
            case BallColor.ORANGE:
                ChosenColor = OrangeColor;
                break;
            case BallColor.PINK:
                ChosenColor = PinkColor;
                break;
            case BallColor.PURPLE:
                ChosenColor = PurpleColor;
                break;
        }
    }
}
