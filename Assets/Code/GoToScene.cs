﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoToScene : MonoBehaviour
{
    public void NextScene()
    {
        int thisSceneInd = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(thisSceneInd + 1);
    }

    public void SceneWithIndex(int index)
    {

    }
}
