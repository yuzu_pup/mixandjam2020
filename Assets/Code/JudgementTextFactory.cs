﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JudgementTextFactory : MonoBehaviour
{
    public static JudgementTextFactory Instance;
    public GameObject Prefab;
    public Transform Parent;

    private void Awake()
    {
        Instance = this;
    }

    public static void MakeJudgementText(Vector3 worldPos, string text)
    {
        Instance.MakeJudgementTextIntl(worldPos, text);
    }

    private void MakeJudgementTextIntl(Vector3 worldPos, string text)
    {
        GameObject inst = Instantiate(Prefab, worldPos, Quaternion.identity);
        inst.transform.SetParent(Parent);
        inst.GetComponent<JudgementText>().Text.text = text;
    }
}
