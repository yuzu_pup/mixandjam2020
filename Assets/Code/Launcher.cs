﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class Launcher : MonoBehaviour
{
    public BeatJudger Judger;
    public Image Image;
    public Sprite BlueSpr;
    public Sprite OrangeSpr;
    public Sprite PinkSpr;
    public Sprite PurpleSpr;


    //ball launcher needs to rotate in accordance to the mouse, queue a ball that can switch and shoot ball on click.
    public List<BallColor> LoadedBallColors = new List<BallColor>();
    public Ball LoadedBall;
    public int ActiveNum = 0;
    public BallColor ActiveBallColor=> LoadedBallColors[ActiveNum];

    public Vector3 CursorSpot;
    public float FireSpeed = 50f;
    public Vector3 LauncherPos;
    public float UpRotationOffsetInDegrees;
    public Transform LoadedBallLocalOffset;
    public Transform LoadedBallParent;
    public Transform FiredBallParent;

    // Start is called before the first frame update
    void Start()
    {

        LoadedBallColors.AddRange(new[] {BallColor.PURPLE, BallColor.BLUE, BallColor.PINK, BallColor.ORANGE});
        LoadedBall = SpawnBall(BallColor.PURPLE);
        ChangeSelectedColor();
        SetColorOfLoadedBallToSelectedColor();
    }

    // Update is called once per frame


    void Update()
    {
        LauncherPos = transform.position;
        CursorSpot = Input.mousePosition;


        Vector3 dirToCursor=(CursorSpot-LauncherPos);
        dirToCursor.z = 0;
        Debug.DrawRay(transform.position, dirToCursor);
        dirToCursor.Normalize();
        float rotFromZero = UpRotationOffsetInDegrees+(Vector3.SignedAngle(Vector3.up, dirToCursor, Vector3.forward));
        transform.rotation = Quaternion.Euler(0, 0, rotFromZero);

        bool didPerformAnInput = Input.GetMouseButtonDown(0) || Input.GetMouseButtonDown(1);
        if(didPerformAnInput)
        {
            BeatJudger.Judgement j = Judger.GetJudgement();
            string t;
            switch(j)
            {
                default:
                case BeatJudger.Judgement.Great:
                    t = "Great!";
                    break;
                case BeatJudger.Judgement.Good:
                    t = "Good!";
                    break;
                case BeatJudger.Judgement.Bad:
                    t = "Bad...";
                    break;
            }
            JudgementTextFactory.MakeJudgementText(CursorSpot, t);

            //this will shoot the current ball
            if (Input.GetMouseButtonDown(0))
            {
                ShootBall(dirToCursor);
            }

            //this will swap the current ball
            if (Input.GetMouseButtonDown(1))
            {
                Sounds.PlaySound(SoundName.ChangeColor);
                ChangeSelectedColor();
                SetColorOfLoadedBallToSelectedColor();
            }

            if (j == BeatJudger.Judgement.Bad)
            {
                Scoring.ResetMultiplier();
            }
            else
            {
                Scoring.IncrementMultiplier();
            }
        }
    }


    private Ball SpawnBall(BallColor color)
    {
        return BallFactory.MakeBall(LoadedBallLocalOffset.position, LoadedBallParent, color); ;
    }
    private void ShootBall(Vector3 direction)
    {
        Sounds.PlaySound(SoundName.LaunchBall);

        //fire active ball/angle
        LoadedBall.transform.SetParent(FiredBallParent);
        LoadedBall.Shoot(direction*FireSpeed);
        //load new ball
        ChangeSelectedColor();
        LoadedBall = SpawnBall(ActiveBallColor);
    }

    //this will change the ball to the next in the color list
    //good for adding into both left and right as it should cycle if shot

    public void ChangeSelectedColor()
    {
  
        ActiveNum = (ActiveNum + 1) % LoadedBallColors.Count;
        Lights.SetColors(ActiveBallColor);
        SetSprite(ActiveBallColor);
    }
    public void SetColorOfLoadedBallToSelectedColor()
    {
        LoadedBall.SetColor(ActiveBallColor);
    }
    private void SetSprite(BallColor color)
    {
        switch (color)
        {
            case BallColor.BLUE:
                Image.sprite = BlueSpr;
                break;
            case BallColor.ORANGE:
                Image.sprite = OrangeSpr;
                break;
            case BallColor.PINK:
                Image.sprite = PinkSpr;
                break;
            case BallColor.PURPLE:
                Image.sprite = PurpleSpr;
                break;
        }
    }
}
