﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public enum CheerType {Great, Good, Bad};

public class Cheer : MonoBehaviour
{
    public static Cheer Instance;

    public static void StartCheer(CheerType cheer)
    {
        Instance.StartCheerIntl(cheer);
    }

    public bool CheerCurrentlyActive;
    public Transform StartPos;
    public Transform EndPos;
    public Image Image;
    public float MoveTime;
    public float WaitTime;
    public float WobbleAngle;
    public float WobbleTime;
    public Sprite GreatSpr;
    public Sprite GoodSpr;
    public Sprite BadSpr;
    public bool TestGreat;
    public bool TestGood;
    public bool TestBad;
    public Tween RotateTween;

    void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (TestGreat)
        {
            TestGreat = false;
            StartCheer(CheerType.Great);
        }
        if (TestGood)
        {
            TestGood = false;
            StartCheer(CheerType.Good);
        }
        if (TestBad)
        {
            TestBad = false;
            StartCheer(CheerType.Bad);
        }
    }
    public void StartCheerIntl(CheerType cheer)
    {
        if(CheerCurrentlyActive)
        {
            return;
        }
        CheerCurrentlyActive = true;

        Image.transform.rotation = Quaternion.identity;
        RotateTween?.Kill();
        SetSprite(cheer);
        Sequence sequence = DOTween.Sequence();

        //this is the instructions for the sprite to move into position and Shake
        sequence.Append(
            Image.transform.DOMove(EndPos.transform.position, MoveTime)
                .From(StartPos.transform.position)
        );
        if (cheer == CheerType.Good)
        {
            RotateTween= Image.transform.DORotate(new Vector3(0, 0, WobbleAngle), WobbleTime)
                            .From(new Vector3(0, 0, -WobbleAngle))
                            .SetEase(Ease.InOutQuad)
                            .SetLoops(-1, LoopType.Yoyo)
                            .Play();
        }
        if (cheer == CheerType.Great)
        {
            RotateTween= Image.transform.DORotate(new Vector3(0, 0, WobbleAngle), WobbleTime)
                            .From(new Vector3(0, 0, -WobbleAngle))
                            .SetEase(Ease.InOutQuad)
                            .SetLoops(-1, LoopType.Yoyo)
                            .Play();
        }else { };

        sequence.Append(
            Image.transform.DOMove(StartPos.transform.position, MoveTime).SetDelay(WaitTime)
        );

        sequence.Play();
        sequence.OnComplete(() => { CheerCurrentlyActive = false; });
    }

    private void SetSprite(CheerType cheer)
    {
        switch (cheer)
        {
            case CheerType.Great:
                Sounds.PlaySound(SoundName.Great);
                Image.sprite = GreatSpr;
                break;
            case CheerType.Good:
                Sounds.PlaySound(SoundName.Good);
                Image.sprite = GoodSpr;
                break;
            case CheerType.Bad:
                Sounds.PlaySound(SoundName.Bad);
                Image.sprite = BadSpr;
                break;

        }
    }
}
