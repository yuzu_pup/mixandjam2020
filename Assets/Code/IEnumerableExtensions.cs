﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class IEnumerableExtensions
{
    public static T[] RandomItems<T>(this IEnumerable<T> container, int count)
    {
        List<T> list = container as List<T>;
        if(list == null)
        {
            list = new List<T>(container);
        }

        T[] ret = new T[count];
        for(int ii = 0; ii < count; ii++)
        {
            ret[ii] = list[UnityEngine.Random.Range(0, list.Count)];
        }
        return ret;
    }
}
