﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SoundName
{
    // Vox
    Bad,
    Good,
    Great,
    ThanksForPlaying,
    StartGameVox,
    // FX 
    LaunchBall,
    ChangeColor,
    MatchMade,
    MultiplierUp,
    MultiplierLost
}
public class Sounds : MonoBehaviour
{
    public static Sounds Instance;

    public AudioSource VoxSource;
    public AudioSource FXSource;

    public SingleSound StartGameVox;
    public VariableSoundBag BadSoundBag;
    public VariableSoundBag GoodSoundBag;
    public VariableSoundBag GreatSoundBag;
    public SingleSound ThanksForPlayingSound;

    public SingleSound LaunchBallSound;
    public SingleSound ChangeColorSound;
    public SingleSound MatchMadeSound;
    public SingleSound MultiplierUpSound;
    public SingleSound MultiplierLostSound;


    private Dictionary<SoundName, IPlayableSound> NameToPlayable;

    public static void PlaySound(SoundName name)
    {
        Instance.PlaySoundIntl(name);
    }

    public static void StopSound(SoundName name)
    {
        Instance.StopSoundIntl(name);
    }

    private void Awake()
    {
        Instance = this;
        BadSoundBag.Configure(VoxSource);
        GoodSoundBag.Configure(VoxSource);
        GreatSoundBag.Configure(VoxSource);
        ThanksForPlayingSound.Configure(VoxSource);
        StartGameVox.Configure(VoxSource);

        LaunchBallSound.Configure(FXSource);
        ChangeColorSound.Configure(FXSource);
        MatchMadeSound.Configure(FXSource);
        MultiplierUpSound.Configure(FXSource);
        MultiplierLostSound.Configure(FXSource);

        NameToPlayable = new Dictionary<SoundName, IPlayableSound>
        {
            // Vox
            { SoundName.Bad, BadSoundBag },
            { SoundName.Good, GoodSoundBag },
            { SoundName.Great, GreatSoundBag },
            { SoundName.ThanksForPlaying, ThanksForPlayingSound },
            { SoundName.StartGameVox, StartGameVox },

            // FX
            { SoundName.LaunchBall, LaunchBallSound },
            { SoundName.ChangeColor, ChangeColorSound },
            { SoundName.MatchMade, MatchMadeSound },
            { SoundName.MultiplierUp, MultiplierUpSound },
            { SoundName.MultiplierLost, MultiplierLostSound }
        };
    }

    public void PlaySoundByStringName(string name)
    {
        PlaySound((SoundName) System.Enum.Parse(typeof(SoundName), name));
    }

    public void PlaySoundIntl(SoundName name)
    {
        NameToPlayable[name].Play();
    }

    public void StopSoundIntl(SoundName name)
    {
        NameToPlayable[name].Stop();
    }

}
