﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleSound : MonoBehaviour, IPlayableSound
{
    [HideInInspector]
    public AudioSource Source;
    public AudioClip Sound;

    public void Configure(AudioSource source)
    {
        Source = source;
    }

    public void Play()
    {
        Source.Stop();
        Source.clip = Sound;
        Source.Play();
    }

    public void Stop()
    {
        Source.Stop();
    }
}
