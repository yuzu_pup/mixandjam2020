﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class EndScreen : MonoBehaviour
{

    public TMPro.TextMeshProUGUI Text;
    // Start is called before the first frame update
    void Start()
    {
        Text.text = Scoring.Instance.TotalScore.ToString();
        Sounds.PlaySound(SoundName.ThanksForPlaying);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Music.Stop();
            SceneManager.LoadScene (0);
        }
    }
}
