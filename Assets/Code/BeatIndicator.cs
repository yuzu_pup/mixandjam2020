﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class BeatIndicator : MonoBehaviour
{
    public BeatJudger Judger;
    public Image Target;
    public Image Indicator;
    
    void Start()
    {
        Indicator.transform.DOMove(Target.transform.position, Beat.Instance.SecondsPerBeat)
            .SetLoops(-1)
            .SetEase(Ease.Linear)
            .Play();
    }

    void Update()
    {
        switch(Judger.GetJudgement())
        {
            case BeatJudger.Judgement.Great:
                Indicator.color = Color.green;
                break;
            case BeatJudger.Judgement.Good:
                Indicator.color = Color.yellow;
                break;
            case BeatJudger.Judgement.Bad:
                Indicator.color = Color.red;
                break;
        }
    }
}
