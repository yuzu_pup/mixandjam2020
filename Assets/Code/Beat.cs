﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Beat : MonoBehaviour
{
    public static Beat Instance;

    public float VisualDelay;
    public float BPM;
    public float SecondsPerBeat => 1 / BPM * 60;
    public float TimeToNearestBeat => SecondsSinceLastBeat > (SecondsPerBeat / 2) ? SecondsPerBeat - SecondsSinceLastBeat : SecondsSinceLastBeat;
    [HideInInspector]
    public float SecondsSinceLastBeat;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {

    }

    void Update()
    {
        SecondsSinceLastBeat += Time.deltaTime;
        if (SecondsSinceLastBeat > SecondsPerBeat)
        {
            SecondsSinceLastBeat = SecondsSinceLastBeat - SecondsPerBeat;
        }
    }
}
