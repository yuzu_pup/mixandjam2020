﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scoring : MonoBehaviour
{
    public static Scoring Instance;
    public int TotalScore;
    public int MidScore;
    public int FramesToAnimateToFinalScore;
    public int ScorePerFrame;
    public float Multiplier;
    public TMPro.TextMeshProUGUI ScoreText;
    public TMPro.TextMeshProUGUI MultiplierText;
    public int DisplayScore;
    public int MatchAmount;
    public float MultiplierIncrementPerSuccess;

    public static void AddScore(int score)
    {
        Instance.AddScoreInternal(score);
    }

    public void AddScoreInternal(int score)
    {
        MidScore = (score * (int)Multiplier);
    }

    private void Awake()
    {
        Instance = this;
        ScoreText.text = "0";
        MultiplierText.text = "1x";
    }

    public static void IncrementMultiplier()
    {
        Instance.IncrementMultiplierInternal();
    }
        
    public void IncrementMultiplierInternal()
    {
        float lastMulti = Multiplier;
        Multiplier = Mathf.Min(Multiplier + MultiplierIncrementPerSuccess, 4);
        if (lastMulti < 2f && Multiplier >= 2f)
        {
            Sounds.PlaySound(SoundName.MultiplierUp);
            Cheer.StartCheer(CheerType.Good);
        }
        if (lastMulti < 4f && Multiplier >= 4f)
        {
            Sounds.PlaySound(SoundName.MultiplierUp);
            Cheer.StartCheer(CheerType.Great);
        }
        MultiplierText.text = $"{ ((int)Multiplier).ToString() }x";
    }
    public static void ResetMultiplier()
    {
        Instance.ResetMultiplierInternal();
    }
    public void ResetMultiplierInternal()
    {
        Sounds.PlaySound(SoundName.MultiplierLost);
        Cheer.StartCheer(CheerType.Bad);
        Multiplier = 1;
        MultiplierText.text = $"{ ((int)Multiplier).ToString() }x";
    }
    public static int MatchCount(int count)
    {
        return (Instance.MatchCountInternal(count));
    }
    public int MatchCountInternal(int count)
    {
        TotalScore += ((count - 2) * MidScore);
        ScorePerFrame = (TotalScore - DisplayScore) / FramesToAnimateToFinalScore;
        return ((count-2)*MidScore);
    }

    // Update is called once per frame
    void Update()
    {
        if (DisplayScore != TotalScore)
        {
            DisplayScore += ScorePerFrame;
            if(DisplayScore > TotalScore)
            {
                DisplayScore = TotalScore;
            }

            ScoreText.text = DisplayScore.ToString();
        }
    }
}

