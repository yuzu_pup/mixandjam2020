﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class BallLine : MonoBehaviour
{
    public bool UseDebugLine;
    public int InitialLineSize;
    public BallColor[] DebugLine;
    public List<BallColor> InitialLine = new List<BallColor>();

    public BeatJudger Judger;
    public float BeatLowSpeed;
    public float BeatHighSpeed;
    public Tween SpeedTween;
    public int MatchSize;

    public float BallSpeed = 80f;
    public Spline2DComponent Spline;
    public float DistanceBetweenBalls;
    public List<Ball> Balls = new List<Ball>();

    private void Init()
    {
        DistanceBetweenBalls = ((RectTransform) BallFactory.Instance.BallPrefab.transform).rect.width;

        if(UseDebugLine)
        {
            InitialLine.AddRange(DebugLine);
        }
        else
        {
            InitialLine = new List<BallColor>((new List<BallColor> { BallColor.ORANGE, BallColor.PINK, BallColor.BLUE, BallColor.PURPLE}).RandomItems(InitialLineSize));
        }

        foreach (BallColor c in InitialLine)
        {
            Balls.Add(BallFactory.MakeBall(Vector3.zero, transform, c));
        }

        for (int ii = 0; ii < Balls.Count; ii++)
        {
            Ball b = Balls[ii];
            b.IsOnPath = true;
            b.FiredCollided += FiredBallCollided;
            b.DistanceDownPath += DistanceBetweenBalls * (Balls.Count - ii);
            SyncBallWithPath(b);
        }

        SpeedTween = DOTween.To((t) => BallSpeed = t, BeatLowSpeed, BeatHighSpeed, Beat.Instance.SecondsPerBeat / 2)
            .SetEase(Ease.InOutCubic)
            .SetLoops(-1, LoopType.Yoyo);
    }

    // Start is called before the first frame update
    void Start()
    {
        Init();
    }

    // Update is called once per frame
    void Update()
    {
        if(Music.IsPlaying() == false)
        {
            Music.Play();
        }

        if (Balls.Count < 1)
        {
            SceneManager.LoadScene(2);
        }


        for (int ii = 0; ii < Balls.Count; ii++)
        {
            Ball b = Balls[ii];
            bool shouldMove = false;
            if (ii == Balls.Count - 1)
            {
                shouldMove = true;
            }
            else
            {
                Ball previous = Balls[ii + 1];
                shouldMove = Mathf.Abs(b.DistanceDownPath - previous.DistanceDownPath) < DistanceBetweenBalls;
            }
            if (shouldMove)
            {
                b.DistanceDownPath += BallSpeed * Time.deltaTime;
                if(b.DistanceDownPath >= Spline.Length)
                {
                    SceneManager.LoadScene(2);
                }
                SyncBallWithPath(b);
            }
        }
    }

    public void FiredBallCollided(Ball who, Ball other, CollisionDirection dir)
    {
        int indOfWho = Balls.IndexOf(who);
        if(dir == CollisionDirection.Front)
        {
            InsertBall(indOfWho, other);
        }
        else
        {
            InsertBall(Mathf.Min(indOfWho + 1, Balls.Count), other);
        }
    }

    public void InsertBall(int index, Ball ball)
    {
        ball.DistanceDownPath = Balls[index].DistanceDownPath;
        Balls.Insert(index, ball);
        ball.IsOnPath = true;
        ball.FiredCollided += FiredBallCollided;
        PushBallsForward(index, DistanceBetweenBalls);

        List<Ball> matches = new List<Ball>() { ball };
        // Look left
        matches.AddRange(GetMatchingBalls(index, ball.Color, -1));
        // Look right
        matches.AddRange(GetMatchingBalls(index, ball.Color, 1));

        if (matches.Count >= 3)
        {
            Sounds.PlaySound(SoundName.MatchMade);
            int score = Scoring.MatchCount(matches.Count);
            JudgementTextFactory.MakeJudgementText(matches[0].transform.position, score.ToString());

            foreach (Ball m in matches)
            {
                DestroyBall(m);
            }
        }
    }

    public List<Ball> GetMatchingBalls(int startingIndex, BallColor color, int direction)
    {
        List<Ball> matches = new List<Ball>();

        int currIndex = startingIndex + direction;
        if (currIndex < 0 || currIndex >= Balls.Count)
        {
            return matches;
        }

        Ball neighbor = Balls[currIndex];
        BallColor neighborColor = neighbor.Color;
        bool colorsMatch = color == neighborColor;

        while (colorsMatch)
        {
            matches.Add(neighbor);
            currIndex += direction;
            if (currIndex < 0 || currIndex >= Balls.Count)
            {
                return matches;
            }
            neighbor = Balls[currIndex];
            neighborColor = Balls[currIndex].Color;
            colorsMatch = color == neighborColor;
        }
        return matches;
    }

    private void DestroyBall(Ball b)
    {
        Destroy(b.gameObject);
        Balls.Remove(b);
    }

    private void PushBallsForward(int fromIndex, float distance)
    {
        for(int ii = fromIndex; ii >= 0; ii--)
        {
            Ball b = Balls[ii];
            if (ii > 0)
            {
                Ball nextBall = Balls[ii - 1];
                if ((nextBall.DistanceDownPath - b.DistanceDownPath) > DistanceBetweenBalls + 1f)
                    break;
            }
            b.DistanceDownPath += distance;
        }
        UpdateBallsNow();
    }

    private void UpdateBallsNow()
    {
        foreach(Ball b in Balls)
        {
            SyncBallWithPath(b);
        }
    }

    private void SyncBallWithPath(Ball b)
    {
        b.transform.position = Spline.InterpolateDistanceWorldSpace(b.DistanceDownPath);
        b.ForwardDir = Spline.DerivativeDistanceWorldSpace(b.DistanceDownPath).normalized;
    }
}
