﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour
{
    public static Music Instance;

    public AudioSource Song;

    void Awake()
    {
        if(Instance != null)
        {
            Destroy(this);
        }
        Instance = this;
    }


    public static void Play()
    {
        Instance.PlayIntl();
    }

    public void PlayIntl()
    {
        Song.Play();
    }

    public static bool IsPlaying()
    {
        return Instance.IsPlayingIntl();
    }

    public bool IsPlayingIntl()
    {
        return Song.isPlaying;
    }
    public static void Stop()
    {
        Instance.StopIntl();
    }
    public void StopIntl()
    {
        Song.Stop();
    }
}
