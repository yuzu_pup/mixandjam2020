﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallFactory : MonoBehaviour
{
    public static BallFactory Instance;

    public GameObject BallPrefab;

    void Awake()
    {
        Instance = this;
    }

    public static Ball MakeBall(Vector3 worldPos, Transform parent, BallColor color)
    {
        return Instance.MakeBallIntl(worldPos, parent, color);
    }

    public Ball MakeBallIntl(Vector3 worldPos, Transform parent, BallColor color)
    {
        GameObject go = Instantiate(BallPrefab, parent);
        go.transform.position = worldPos;
        go.transform.SetParent(parent);
        Ball b = go.GetComponent<Ball>();
        b.SetColor(color);
        return b;
    }
}
