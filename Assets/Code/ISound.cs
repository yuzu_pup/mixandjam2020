﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayableSound
{
    void Configure(AudioSource source);
    void Play();
    void Stop();
}
