﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class JudgementText : MonoBehaviour
{
    public float Lifetime;
    public float MoveDistance;
    public float HeightOffset;
    public TextMeshProUGUI Text;

    // Start is called before the first frame update
    void Start()
    {
        Text.DOFade(0f, Lifetime * 0.8f).OnComplete(() =>
        {
            Destroy(gameObject);
        }).SetDelay(Lifetime * 0.2f);
        transform.DOMove(new Vector2(transform.position.x, transform.position.y-MoveDistance), Lifetime)
            .From(new Vector2(transform.position.x, transform.position.y + HeightOffset));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
