﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum BallColor { BLUE, ORANGE, PURPLE, PINK }

public enum CollisionDirection { Front, Behind }

public class Ball : MonoBehaviour
{

    public delegate void FiredBallCollided(Ball me, Ball fired, CollisionDirection dir);

    public float DistanceDownPath;
    public bool IsOnPath;
    public Vector3 ForwardDir;
    public BallColor Color;
    public Image Image;
    public Sprite BlueSpr;
    public Sprite OrangeSpr;
    public Sprite PinkSpr;
    public Sprite PurpleSpr;
    public Vector2 Velocity;

    public event FiredBallCollided FiredCollided;

    private void Start()
    {
        SetSprite(Color);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject other = collision.gameObject;
        Ball otherBall = other.GetComponent<Ball>();
        if (otherBall)
        {
            if (IsOnPath && otherBall.IsOnPath == false)
            {
                FiredCollided?.Invoke(this, otherBall, PointIsInFrontOf(otherBall.transform.position) ? CollisionDirection.Front : CollisionDirection.Behind);
            }
        }
    }

    public void SetColor(BallColor color)
    {
        Color = color;
        SetSprite(color);
    }
    private void SetSprite(BallColor color)
    {
        switch (color)
        {
            case BallColor.BLUE:
                Image.sprite = BlueSpr;
                break;
            case BallColor.ORANGE:
                Image.sprite = OrangeSpr;
                break;
            case BallColor.PINK:
                Image.sprite = PinkSpr;
                break;
            case BallColor.PURPLE:
                Image.sprite = PurpleSpr;
                break;
        }
    }
    public void Shoot(Vector2 velocity)
    {
        IsOnPath = false;
        Velocity = velocity;
    }
    private void Update()
    {
        if(!IsOnPath)
        {
            transform.Translate(Velocity * Time.deltaTime, Space.World);
        }
        else
        {
            //Debug.DrawRay(transform.position, ForwardDir * 10, UnityEngine.Color.red);
        }
    }

    public override string ToString()
    {
        return Color.ToString();
    }

    public bool PointIsInFrontOf(Vector3 worldPos)
    {
        Vector3 meToPoint = worldPos - transform.position;
        return Vector3.Dot(ForwardDir, meToPoint) > 0;
    }
}
