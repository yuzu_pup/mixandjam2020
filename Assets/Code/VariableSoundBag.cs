﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VariableSoundBag : MonoBehaviour, IPlayableSound
{
    [HideInInspector]
    public AudioSource Source;
    public AudioClip[] Options;
    [Range(0f, 1f)]
    public float ProbabilityToPlaySound = 1f;


    public void Configure(AudioSource source)
    {
        Source = source;
    }

    public void Play()
    {
        PlayRandomSound();
    }

    public void Stop()
    {
        Source.Stop();
    }

    public void PlayRandomSound()
    {
        if(UnityEngine.Random.value < ProbabilityToPlaySound)
        {
            Source.Stop();
            Source.clip = Options.RandomItems(1)[0];
            Source.Play();
        }
    }
}
